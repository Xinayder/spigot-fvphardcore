package dev.xinayder.fvphardcore

import java.io.File
import java.io.IOException
import java.util.Date
import java.util.logging.Logger

import org.bukkit.Bukkit
import org.bukkit.BanList
import org.bukkit.entity.Player
import org.bukkit.configuration.InvalidConfigurationException
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.event.EventHandler

public class Main() : org.bukkit.plugin.java.JavaPlugin(), org.bukkit.event.Listener {

    private lateinit var deathCfgFile : File
    private lateinit var deathCfg : FileConfiguration

    companion object {
        public val logger : Logger = Bukkit.getLogger();
    }

    override fun onEnable() {
        if (getServer().isHardcore()) {
            Bukkit.getPluginManager().registerEvents(this, this)
            createDeathCfg()
        } else {
            logger.warning("Disabling plugin as server isn't in hardcore mode")
            this.setEnabled(false)
        }
    }

    private fun createDeathCfg() {
        deathCfgFile = File(getDataFolder(), "deaths.yml")
        
        if (!deathCfgFile.exists()) {
            deathCfgFile.getParentFile().mkdirs()
            deathCfgFile.createNewFile()
        }

        deathCfg = YamlConfiguration()
        try {
            deathCfg.load(deathCfgFile)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: InvalidConfigurationException) {
            e.printStackTrace()
        }
    }

    public fun getDeathConfig() : FileConfiguration {
        return deathCfg
    } 

    public fun saveDeathConfig() {
        deathCfg.save(deathCfgFile)
    }

    @EventHandler
    public fun onPlayerDeathEvent(event: org.bukkit.event.entity.PlayerDeathEvent) {
        var player : Player = event.getEntity()
        var playerName : String = player.getPlayerListName()
        var playerDeathsKey : String = playerName + ".deaths"
        var playerDeathTimeKey : String = playerName + ".death-time"
        var playerUnbanTimeKey : String = playerName + ".unban-time"

        if (deathCfg.get(playerName) == null) {
            deathCfg.set(playerDeathsKey, 1)
        }
        else {
            var deathCount : Int = deathCfg.getInt(playerDeathsKey)

            deathCfg.set(playerDeathsKey, deathCount + 1)
        }

        var deathTimeEpoch : Long = System.currentTimeMillis()
        var unbanTimeEpoch : Long = deathTimeEpoch + 86400000
        var unbanTime : Date = Date(unbanTimeEpoch)

        deathCfg.set(playerDeathTimeKey, deathTimeEpoch)
        deathCfg.set(playerUnbanTimeKey, unbanTimeEpoch)
        saveDeathConfig()

        Bukkit.getBanList(BanList.Type.NAME).addBan(playerName, "Hardcore death", unbanTime, "FVPHardcore")

        player.kickPlayer("Você foi banido por morrer no modo §4hardcore§f.")

    }

    @EventHandler
    public fun onPlayerLoginEvent(event: org.bukkit.event.player.PlayerLoginEvent) {
        var banlist = Bukkit.getBanList(BanList.Type.NAME)
        var player : Player = event.getPlayer()
        var playerName : String = event.getPlayer().getPlayerListName()
        var playerDeathTimeKey : String = playerName + ".death-time"
        var playerUnbanTimeKey : String = playerName + ".unban-time"

        var playerBanEntry = banlist.getBanEntry(playerName)

        // If the player is banned, check if there's a ban entry
        // and if there's an expiration date
        if (event.getResult() == org.bukkit.event.player.PlayerLoginEvent.Result.KICK_BANNED) {
            if (playerBanEntry != null) {
                if (playerBanEntry.getExpiration() != null) {
                    // Calculate the time left for the player to be
                    // unbanned
                    var currentTime : Long = System.currentTimeMillis()
                    var unbanTime : Long = playerBanEntry.getExpiration()!!.getTime()

                    var timeLeft : Long = unbanTime - currentTime
                    var secs : Long = timeLeft / 1000 % 60
                    var mins : Long = timeLeft / (60 * 1000) % 60
                    var hours : Long = timeLeft / (60 * 60 * 1000) % 24
                    var days : Long = timeLeft / (24 * 60 * 60 * 1000)

                    var sb : StringBuilder = StringBuilder()
                    if (days > 0) {
                        sb.append(days)
                        sb.append("d ")
                    }
                    sb.append(hours)
                    sb.append("h ")
                    sb.append(mins)
                    sb.append("m ")
                    sb.append(secs)
                    sb.append("s")

                    event.setKickMessage("Você foi banido por morrer no modo §4hardcore§f.\nVocê será desbanido em: §6" + sb.toString())
                }
            }
        }
    }

    @EventHandler
    public fun onPlayerGameModeChangeEvent(event: org.bukkit.event.player.PlayerGameModeChangeEvent) {
        // In Hardcore mode, whenever someone dies, their gamemode
        // is changed to Spectator. We don't want that.
        if (event.newGameMode == org.bukkit.GameMode.SPECTATOR) {
            event.setCancelled(true)
        }
    }
}